from django.db import models

class TypeReaction(models.Model):
    typereaction_id = models.AutoField(primary_key=True)
    typereaction_name = models.CharField('TypeReaction_Name', max_length = 100)
    