from django.db import models
from .user import User
from .post import Post
from .typereaction import TypeReaction

class Reaction(models.Model):
    reaction_id = models.AutoField(primary_key=True)
    reaction_creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    
    reaction_post_id = models.ForeignKey(Post, on_delete=models.CASCADE)
    reaction_user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    reaction_typereaction_id=models.ForeignKey(TypeReaction, on_delete=models.CASCADE)
    