from rest_framework import generics

from funpetsApp.models.typepost import TypePost
from funpetsApp.serializers.typepostSerializer import TypePostSerializer

class TypePostDetailView(generics.RetrieveAPIView):
    queryset = TypePost.objects.all()
    serializer_class = TypePostSerializer

    def get(self, request, *args, **kwargs):
        
        return super().get(request, *args, **kwargs)