from rest_framework import status, views
from rest_framework.response import Response

from funpetsApp.serializers.typepostSerializer import TypePostSerializer

class TypePostCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = TypePostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        stringResponse = {'created record': serializer.data['typepost_id']}         
        return Response(stringResponse, status=status.HTTP_201_CREATED)