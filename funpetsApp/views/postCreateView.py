from rest_framework import status, views
from rest_framework.response import Response

from funpetsApp.serializers.postSerializer import PostSerializer

from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend

class PostCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = PostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        
        if valid_data['user_id'] != kwargs['upk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        
        stringResponse = {'created record': serializer.data['post_id']}
        return Response(stringResponse, status=status.HTTP_201_CREATED)