from rest_framework import generics

from funpetsApp.models.typepet import TypePet
from funpetsApp.serializers.typepetSerializer import TypePetSerializer

class TypePetDetailView(generics.RetrieveAPIView):
    queryset = TypePet.objects.all()
    serializer_class = TypePetSerializer

    def get(self, request, *args, **kwargs):

        return super().get(request, *args, **kwargs)