from django.db.models import fields
from funpetsApp.models.follower import Follower
from rest_framework import serializers

class FollowerSerializer(serializers.ModelSerializer):
    class meta:
        model = Follower
        fields = [
            'follower_id',
            'follower_creation_date',
            'follower_account_id',
            'follower_user_id'
        ]
