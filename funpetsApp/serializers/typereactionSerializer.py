from funpetsApp.models.typereaction import TypeReaction
from rest_framework import serializers

class TypeReactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeReaction
        fields = ['typereaction_id', 'typereaction_name']
        