from funpetsApp.models.account import Account
from rest_framework import serializers
class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = [
            'account_id',
            'account_name',
            'account_nickname',
            'account_image',
            'account_description',
            'account_birthdate',
            'account_creationdate',
            'account_user_id',
            'account_typepet_id'
            ]