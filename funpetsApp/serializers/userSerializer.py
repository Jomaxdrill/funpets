from rest_framework import serializers
from funpetsApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'last_name', 'nickname', 
                    'email', 'phone', 'description', 'birthdate', 'creationdate']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id': user.id,
            'name': user.name,
            'last_name': user.last_name,
            'nickname': user.nickname,
            'email' : user.email,
            'phone' : user.phone,
            'descripcion' : user.description,
            'birthdate': user.birthdate,
            'creationdate' : user.creationdate
        }

