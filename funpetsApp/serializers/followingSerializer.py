from django.db.models import fields
from funpetsApp.models.following import Following
from rest_framework import serializers

class FollowingSerializer(serializers.ModelSerializer):
    class meta:
        model = Following
        fields = [
            'following_id',
            'following_creation_date',
            'following_user_id',
            'following_account_id'
        ]