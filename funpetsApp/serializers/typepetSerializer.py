from funpetsApp.models.typepet import TypePet
from rest_framework import serializers

class TypePetSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypePet
        fields = ['typepet_id', 'typepet_name']
    