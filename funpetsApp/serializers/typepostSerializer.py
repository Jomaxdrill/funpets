from funpetsApp.models.typepost import TypePost
from rest_framework import serializers

class TypePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypePost
        fields = ['typepost_id', 'typepost_name']
        